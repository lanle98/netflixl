import './App.css';
import Navigation from './Components/Navigation';

function App() {
  return (
    <div className='App'>
      <Navigation />
      <h1>netflix</h1>
    </div>
  );
}

export default App;
